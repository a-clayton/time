//  LIBRARIES
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

//  MODULES
import { alerts } from './modules/alerts.vuex'
import { days } from './modules/days.vuex'
import { timeRecords } from './modules/timeRecords.vuex'
import { ui } from './modules/ui.vuex'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  supportCircular: true,
  storage: window.localStorage,
  modules: ['alerts', 'days', 'timeRecords', 'ui']
})

export const store = new Vuex.Store({
  modules: { alerts, days, timeRecords, ui },
  plugins: [vuexLocal.plugin]
})
