export default {

    validateRecord: (_model, _newRecord) => {
        let isValid = true
        const _modelKeys = Object.keys(_model)
        const _testKeys = Object.keys(_newRecord)

        _modelKeys.forEach( (k, i) => {
            if ( k !== _testKeys[i] ) isValid = false
        })

        return isValid
    },

    validateArray: (_model, _newArray) => {
        let isValid = true
        _newArray.forEach(r => {
            if(!this.validateRecord(_model, r)) isValid = false
        })
        return isValid
    },

    uuidv44: () => {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        )
    },

    getData: (url) => {
        return fetch.get(url)
            .then(res =>  res)
            .catch(err => err)
    },

    sendRecord: (url, payload) => {
        return fetch.post(url, payload)
            .then(res => res)
            .catch(err => err)
    },

    deleteRecord: (url, payload) => {
        return fetch.delete(url, payload)
            .then(res => res)
            .catch(err => err)
    }
}