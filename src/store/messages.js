export const messages = {
    error: {
        communication: {
            type: 'danger',
            message: 'Error communicating with the server',
            timeout: true
        },
        validatingKeys: {
            type: 'danger',
            message: 'Error validating record keys',
            timeout: true
        },
        notBoolean: {
            type: 'danger',
            message: `Only accepts boolean values`,
            timeout: true
        }
    },
    success: {
        saving: {
            type: 'success',
            message: 'The record was saved',
            timeout: true
        },
        deleting: {
            type: 'success',
            message: 'The record was deleted',
            timeout: true
        },
        loading: {
            type: 'success',
            message: 'The records where loaded',
            timeout: true
        }
    }
}