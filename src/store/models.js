export const alertModel = {
    type: '', // primary, secodary, sucess, danger, warning, info, light, dark
    message: '', // html string
    timeout: true // disappearing?
}


export const dayModel = {
    date: ''
}


export const timeRecordModel = {
    date: '',
    trId: '',
    projectId: '',
    description: '',
    duration: ''
}