export default {
    reset: (_initialState, _state) => {
        Object.keys(_initialState).forEach( key => _state[key] = _initialState[key] )
    },
    updateArray: (_stateArr, _newArray) => {
            _stateArr = _newArray
    },
    createRecord: (_stateArr, _newRecord) => {
        _stateArr.push(_newRecord)
    },
    updateRecord: (_stateArr, _newRecord) => {
        const _recordToUpdate = _stateArr.find( r => r._uuid === _newRecord._uuid)
        Object.keys(_recordToUpdate).forEach( key => _recordToUpdate[key] = _newRecord[key] )
    },
    deleteRecord: (_stateArr, _deleteRecord) => {
        _stateArr.splice(_stateArr.findIndex( r => r._uuid === _deleteRecord._uuid ), 1)
    }
} 