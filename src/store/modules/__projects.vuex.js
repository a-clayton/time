function initialState () {
    return {
      projects: {
          id: '',
          bqId: '',
          name: '',
          description: '',
      }
    }
  }
  
  export const projects = {
    state: initialState,
    mutations: {
      RESET_PROJECTS (state) {
        const s = initialState()
        Object.keys(s).forEach(key => {
          state[key] = s[key]
        })
      },
      UPDATE_PROJECTS (state, projects) {
        state.projects = projects
      },
      CREATE_PROJECT (state, project) {
        state.projects.push(project)
      },
      UPDATE_PROJECT (state, project) {
        state.projects.forEach((p, i) => {
            if (p.id === project.id){
                state.projects.splice(i,1)
                state.projects.push(project)
            }
        })
      },
      DELETE_PROJECT(state, project) {
        state.projects.forEach((p, i) => {
            if (p.id === project.id){
                state.projects.splice(i,1)
            }
        })
      }
    },
    actions: {
        loadProjects (state) {},

    },
    getters: {
      projects: state => state.projects,

    }
  }
  