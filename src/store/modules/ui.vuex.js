import mutations from '../mutations'
import { messages } from '../messages'

function initialState () {
    return {
      loading: false
    }
}

export const ui = {
    state: initialState,
    mutations: {
        RESET_UI(state){
            mutations.reset(initialState(), state)
        },
        SET_LOADING(state, value) {
            state.loading = value
        }
    },
    actions: {
        setLoading(state, payload){
            if ( payload.typeOf() === Boolean ) state.commit('SET_LOADING', payload)
            else state.dispatch('createAlert', messages.error.notBoolean)
        }
    },
    getters: {
        loading: state => state.loading
    }
}