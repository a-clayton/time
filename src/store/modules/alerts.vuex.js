import mutations from '../mutations'
import actions from '../actions'
import { alertModel } from '../models'
import { messages } from '../messages'

function initialState () {
  return {
    alerts: []
  }
}
  
export const alerts = {
  state: initialState,
  mutations: {
    RESET_ALERTS (state) {
      mutations.reset(initialState(), state)
    },
    CREATE_ALERT (state, alert) {
      mutations.createRecord(state.alerts, alert)
    },
    DELETE_ALERT (state, alert) {
      mutations.deleteRecord(state.alerts, alert)
    }
  },
  actions: {
    createAlert (state, payload) {
      if ( actions.validateRecord(alertModel, payload) ) {
        payload._uuid = actions.uuidv44()
        state.commit('CREATE_ALERT', payload)
      } else {
        state.dispatch('createAlert', messages.error.validatingKeys)
      }
    },
    deleteAlert (state, payload) {
      if ( actions.validateRecord(alertModel, payload) ) {
        state.commit('DELETE_ALERT', payload)
      } else {
        state.dispatch('createAlert', messages.error.validatingKeys)
      }
    }
  },
  getters: {
    alerts: state => state.alerts,
    hasAlerts: state => Boolean(state.alerts.length > 0),
    totalAlerts: state => state.alerts.length
  }
}
