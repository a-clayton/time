import mutations from '../mutations'

function initialState () {
    return {
      days: []
    }
  }
  
  export const days = {
    state: initialState,
    mutations: {
      RESET_DAYS (state) {
        mutations.reset(initialState(), state)
      },
      UPDATE_DAYS (state, days) {
        mutations.updateArray(state.days, days)
      },
      CREATE_DAY (state, day) {
        mutations.createRecord(state.days, day)
      },
      UPDATE_DAY (state, day) {
        mutations.updateRecord(state.days, day)
      },
      DELETE_DAY(state, day) {
        mutations.deleteRecord(state.days, day)
      }
    },
    actions: {

    },
    getters: {
      days: state => state.days
    }
  }
  