import mutations from '../mutations'
import actions from '../actions'
import { timeRecordModel } from '../models'
import { messages } from '../messages'

function initialState () {
  return {
    timeRecords: []
  }
}

export const timeRecords = {
  state: initialState,
  mutations: {
    RESET_TIMERECORDS (state) {
      mutations.reset(initialState(), state)
    },
    UPDATE_TIMERECORDS (state, records) {
      mutations.updateArray(state.timeRecords, records)
    },
    CREATE_TIMERECORD (state, record) {
      mutations.createRecord(state.timeRecords, record)
    },
    UPDATE_TIMERECORD (state, record) {
      mutations.updateRecord(state.timeRecords, record)
    },
    DELETE_TIMERECORD (state, record) {
      mutations.deleteRecord(state.timeRecords, record)
    }
  },
  actions: {

    createTimeRecord (state, payload) {
      state.dispatch('setLoading', true)
      if ( actions.validateRecord(timeRecordModel, payload) ) {
        actions.sendData(`/time-records`, payload)
          .then(res => {
            res._uuid = actions.uuidv44()
            state.dispatch('setLoading', false)
            state.commit('CREATE_TIMERECORD', res)
            state.dispatch('createAlert', messages.success.saving)
          })
          .catch(err => {
            state.dispatch('setLoading', false)
            state.dispatch('createAlert', messages.error.communication)
            console.warn(err)
          })
      } else {
        state.dispatch('setLoading', false)
        state.dispatch('createAlert', messages.error.validatingKeys)
      }
    },

    deleteTimeRecord (state, payload) {
      state.dispatch('setLoading', true)
      const recordToDelete = delete payload._uuid
      if ( actions.validateRecord(timeRecordModel, recordToDelete) ) {
          actions.deleteRecord(`/time-records/${payload.id}`, payload)
            .then( () => {
              state.dispatch('setLoading', false)
              state.commit('DELETE_TIMERECORD', payload)
              state.dispatch('createAlert', messages.success.deleting)
            })
            .catch(err => {
              state.dispatch('setLoading', false)
              state.dispatch('createAlert', messages.error.communication)
              console.warn(err)
            })

      } else {
        state.dispatch('setLoading', false)
        state.dispatch('createAlert', messages.error.validatingKeys)
      }
    },

    loadTimeRecords (state) {
      state.dispatch('setLoading', true)
      actions.getData('/time-records')
        .then(res => {
          if ( actions.validateArray(timeRecordModel, res) ) {
            res.forEach( r => r._uuid = actions.uuidv44() )
            state.dispatch('setLoading', false)
            state.commit('UPDATE_TIMERECORDS', res)
            state.dispatch('createAlert', messages.success.loading)
          } else  {
            state.dispatch('setLoading', false)
            state.dispatch('createAlert', messages.error.validatingKeys)
          }
        })
        .catch(err => {
          state.dispatch('setLoading', false)
          state.dispatch('createAlert', messages.error.communication)
          console.warn(err)
        })
    }
  },
  getters: {
    timeRecords: state => state.timeRecords
  }
}
  